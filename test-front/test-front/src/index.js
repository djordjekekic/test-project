import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes
} from "react-router-dom";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import { Container, Navbar, Nav, Button } from "react-bootstrap";
import Login from "./components/Login/Login";
import { logout } from "./services/auth";
import Glavna from "./components/Projekat/Glavna";
import UnosPolaznika from "./components/Projekat/UnosPolaznika";
import Prijava from "./components/Projekat/Prijava";


class App extends React.Component {
  render() {
    const jwt = window.localStorage['jwt'];
    if (jwt) {
      return (
        <div>
          <Router>
            <Navbar bg="dark" variant="dark" expand>
              <Navbar.Brand as={Link} to="/">
                AUTO SKOLE
              </Navbar.Brand>
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/glavna">
                  POLAZNICI
                </Nav.Link>
              </Nav>

              {window.localStorage['jwt'] ?
                <Button onClick={() => logout()}>Log out</Button> :
                <Nav.Link as={Link} to="/login">Log in</Nav.Link>
              }
            </Navbar>

            <Container style={{ marginTop: 25}}>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/glavna" element={<Glavna />} />
                <Route path="/polaznik/add" element={<UnosPolaznika />} />
                <Route path="/auto-skola/:id/:polaznikId/polaganja" element={<Prijava/>} />
                <Route path="/login" element={<Login />} />
                <Route path="*" element={<NotFound />} />
              </Routes>
            </Container>
          </Router>
        </div>
      );
    }
    else {
      return (
        <div>
          <Router>
            <Navbar bg="dark" variant="dark" expand>
              <Navbar.Brand as={Link} to="/">
              AUTO SKOLE
              </Navbar.Brand>
              {window.localStorage['jwt'] ?
                <Button onClick={() => logout()}>Log out</Button> :
                <Nav.Link as={Link} to="/login">Log in</Nav.Link>
              }
            </Navbar>

            <Container style={{ marginTop: 25 }}>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<Login />} />
                <Route path="*" element={<NotFound />} />
              </Routes>
            </Container>
          </Router>
        </div>
      );
    }
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
