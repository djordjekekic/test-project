import React from "react";
import { Button, Form } from "react-bootstrap";
import { withNavigation, withParams } from "../../routeconf";
import MojAxios from "../../apis/MojAxios";

class UnosPolaznika extends React.Component {

  constructor(props) {
    super(props);

    this.state = { autoSkole: [], ime: "Ime", prezime: "Prezime", godinaRodjenja: 2006, mesto: "Mesto", autoSkolaId: -1 }
  }

  componentDidMount() {
    this.getAutoSkole();
  }

  create = () => {
    var params = {
      'ime': this.state.ime,
      'prezime': this.state.prezime,
      'godinaRodjenja': this.state.godinaRodjenja,
      'mesto': this.state.mesto,
      'autoSkolaId': this.state.autoSkolaId,
    };

    MojAxios.post('/polaznici', params)
      .then(res => {
        // handle success
        console.log(res);

        alert('User was added successfully!');
        this.props.navigate('/glavna');
      })
      .catch(error => {
        // handle error
        console.log(error);
        alert('Error occured please try again!');
      });
  }

  onImeChange = event => {
    console.log(event.target.value);

    const { name, value } = event.target;
    console.log(name + ", " + value);
    this.setState((state, props) => ({
      ime: value
    }));
  }

  onPrezimeChange = event => {
    console.log(event.target.value);

    const { name, value } = event.target;
    console.log(name + ", " + value);

    this.setState((state, props) => ({
      prezime: value
    }));
  }

  onGodinaRodjenjaChange = event => {
    console.log(event.target.value);

    const { name, value } = event.target;
    console.log(name + ", " + value);

    this.setState((state, props) => ({
      godinaRodjenja: value
    }));
  }

  onMestoChange = event => {
    console.log(event.target.value);

    const { name, value } = event.target;
    console.log(name + ", " + value);

    this.setState((state, props) => ({
      mesto: value
    }));
  }

  onAutoSkolaChange = event => {
    console.log(event.target.value);

    const { name, value } = event.target;
    console.log(name + ", " + value);

    this.setState((state, props) => ({
      autoSkolaId: value
    }));
  }

  getAutoSkole() {

    MojAxios.get('/auto-skola')
      .then(res => {
        console.log(res);
        this.setState({ autoSkole: res.data });
      })
      .catch(error => {
        console.log(error);
        alert('Error occured please try again!');
      });

  }

  render() {
    return (
      <div>
        <h1>Unos Polaznika</h1>

        <Form>
          <Form.Group>
            <Form.Label  >Ime</Form.Label>
            <Form.Control as="input" id="ime" type="text" value={this.state.ime} onChange={(e) => this.onImeChange(e)}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label >Prezime</Form.Label>
            <Form.Control as="input" id="prezime" type="text" value={this.state.prezime} onChange={(e) => this.onPrezimeChange(e)}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label >Godina rodjenja</Form.Label>
            <Form.Control as="input" id="godinaRodjenja" type="number" value={this.state.godinaRodjenja} onChange={(e) => this.onGodinaRodjenjaChange(e)}
            ></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label >Mesto</Form.Label>
            <Form.Control as="input" id="mesto" type="text" value={this.state.mesto} onChange={(e) => this.onMestoChange(e)}
            >
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label >Auto skola</Form.Label>
            <Form.Control as="select" id="autoSkolaId" type="text" onChange={(e) => this.onAutoSkolaChange(e)}>
              <option value={-1}>Izaberi auto skolu</option>
              {this.state.autoSkole.map((autoSkola) => {
                return (
                  <option value={autoSkola.id} key={autoSkola.id}>
                    {autoSkola.naziv}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Button disabled={this.state.autoSkolaId<1 || 
            this.state.ime==="Ime" ||  this.state.ime==="" ||
            this.state.prezime==="Prezime" || this.state.prezime==="" ||
            this.state.mesto==="Mesto" || this.state.mesto==="" ||
            this.state.godinaRodjenja>2006} style={{ marginTop: 3 }} 
            onClick={this.create}>
            Kreiraj Polaznika
          </Button>
        </Form>

      </div>
    );
  }
}

export default withNavigation(withParams(UnosPolaznika));