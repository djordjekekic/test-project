import React from "react";
import MojAxios from "../../apis/MojAxios";
import { Table, Button } from "react-bootstrap";
import { withNavigation, withParams } from "../../routeconf";

class Prijava extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            polaganja: []
        };

    }

    componentDidMount() {
        this.getPolaganja(this.props.params.id);
    }

    async getPolaganja(autoSkolaId) {
        try {
            let result = await MojAxios.get("/auto-skola/" + autoSkolaId + "/polaganja/");
            if (result && result.status === 200) {
                this.setState({ polaganja: result.data });
            }
        } catch (error) {
            alert("Nije uspelo dobavljanje.");
        }
    }

    async prijava(polaganjeId,polaznikId) {
        try {
          await MojAxios.put("/polaganje/prijava/" + polaganjeId + "/" + polaznikId);
          var idAutoSkole = this.props.params.id
          await this.getPolaganja(idAutoSkole);
        } catch (error) {
          alert("Nije uspelo menjanje.");
        }
      }

    render() {
        return (
            <div>
                <Table bordered striped style={{ marginTop: 5 }}>
                    <thead className="thead-dark">
                        <tr>
                            <th>Naziv auto skole</th>
                            <th>Datum</th>
                            <th>Broj slobodnih mesta</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.polaganja.map((polaganje) => {
                            if(polaganje.brMesta>0){
                            return (
                                <tr key={polaganje.id}>
                                    <td>{polaganje.autoSkolaNaziv}</td>
                                    <td>{polaganje.datum}</td>
                                    <td>{polaganje.brMesta}</td>
                                    <td>
                                        <Button
                                            variant="info"
                                            onClick={() => this.prijava(polaganje.id,this.props.params.polaznikId)}
                                        >
                                            Prijava
                                        </Button>
                                    </td>
                                </tr>
                            );
                            }
                        })}
                    </tbody>
                </Table>
            </div>
        );

    }

}

export default withNavigation(withParams(Prijava));