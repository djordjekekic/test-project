import React from "react";
import { Table, Button, Form, ButtonGroup, Collapse } from "react-bootstrap";
import { withNavigation } from "../../routeconf";
import MojAxios from "../../apis/MojAxios";


class Glavna extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      polaznici: [],
      pageNo: 0,
      totalPages: 1,
      ispisiPolozio: 'DA',
      search: { autoSkola: -1, imePolaznika: "Ime polaznika" },
      autoSkole: [],
      polaganja: [],
      prijavljeni: []
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData() {
    await this.getAutoSkole();
    await this.getPolaznici(0);
    await this.getPrijavljeni();
  }

  async getPolaznici(page) {

    let config = {
      params: {
        pageNo: page
      }
    };

    if (this.state.search.autoSkola != -1) {
      config.params.autoSkola = this.state.search.autoSkola;
    }

    if (this.state.search.imePolaznika != "Ime polaznika") {
      config.params.imePolaznika = this.state.search.imePolaznika;
    }

    try {
      let result = await MojAxios.get("/polaznici", config);
      if (result && result.status === 200) {
        this.setState({
          pageNo: page,
          polaznici: result.data,
          totalPages: result.headers["total-pages"]
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  goToAdd() {
    this.props.navigate("/polaznik/add");
  }

  async getAutoSkole() {
    try {
      let result = await MojAxios.get("/auto-skola");
      if (result && result.status === 200) {
        this.setState({
          autoSkole: result.data,
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  searchValueInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let search = this.state.search;
    search[name] = value;

    this.setState({ search: search });
  }

  doSearch() {
    this.getPolaznici(0);
  }

  async delete(polaznikId) {
    try {
      await MojAxios.delete("/polaznici/" + polaznikId);
      var nextPage
      if (this.state.pageNo == this.state.totalPages - 1 && this.state.polaznici.length == 1) {
        nextPage = this.state.pageNo - 1
      } else {
        nextPage = this.state.pageNo
      }
      await this.getPolaznici(nextPage);
    } catch (error) {
      alert("Nije uspelo brisanje.");
    }
  }

  async odslusaoTeoriju(polaznikId) {
    try {
      await MojAxios.put("/polaznici/teorija/" + polaznikId);
      var nextPage = this.state.pageNo
      await this.getPolaznici(nextPage);
    } catch (error) {
      alert("Nije uspelo menjanje.");
    }
  }

  async odradioVoznju(polaznikId) {
    try {
      await MojAxios.put("/polaznici/voznja/" + polaznikId);
      var nextPage = this.state.pageNo
      await this.getPolaznici(nextPage);
    } catch (error) {
      alert("Nije uspelo menjanje.");
    }
  }

  async polozio(polaznikId) {
    try {
      await MojAxios.put("/polaznici/polozio/" + polaznikId);
      var nextPage = this.state.pageNo
      await this.getPolaznici(nextPage);
    } catch (error) {
      alert("Nije uspelo menjanje.");
    }
  }

  async getPolaganja(autoSkolaId) {
    try {
      let result = await MojAxios.get("/auto-skola/" + autoSkolaId + "/polaganja/");
      if (result && result.status === 200) {
        this.setState({polaganja: result.data});
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  goToPrijava(autoSkolaId,polaznikId) {
    this.props.navigate("/auto-skola/"+autoSkolaId+"/"+polaznikId+"/polaganja");
  }

  async getPrijavljeni() {
    try {
      let result = await MojAxios.get("/polaganje/prijavljeni");
      if (result && result.status === 200) {
        this.setState({
          prijavljeni: result.data,
        });
      }
    } catch (error) {
      alert("Nije uspelo dobavljanje.");
    }
  }

  

  render() {
    return (
      <div>

        <Form>
          <Form.Group>
            <Form.Label >Auto skola</Form.Label>
            <Form.Control onChange={(event) => this.searchValueInputChange(event)}
              name="autoSkola"
              value={this.state.search.autoSkola}
              as="select" >
              <option value={-1}>Odaberite auto skolu</option>
              {this.state.autoSkole.map((as) => {
                return (
                  <option value={as.id} key={as.id}>
                    {as.naziv}
                  </option>
                );
              })}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label >Ime polaznika</Form.Label>
            <Form.Control value={this.state.search.imePolaznika}
              name="imePolaznika"
              as="input"
              onChange={(event) => this.searchValueInputChange(event)}
            ></Form.Control>
          </Form.Group>
          <Button style={{ margin: 3, width: 90, marginTop: 15 }} onClick={() => this.doSearch()} >
            Pretraga
          </Button>
        </Form>
        {window.localStorage['role'] == "ROLE_ADMIN" ? [
          <Button style={{ margin: 3, width: 150 }} variant="success" onClick={() => this.goToAdd()}>Kreiraj Polaznika</Button>] : null}
        <ButtonGroup style={{ float: "right" }}>
          <Button style={{ margin: 3, width: 90 }}
            disabled={this.state.pageNo == 0} onClick={() => this.getPolaznici(this.state.pageNo - 1)} >
            Prethodna
          </Button>
          <Button style={{ margin: 3, width: 90 }}
            disabled={this.state.pageNo == this.state.totalPages - 1} onClick={() => this.getPolaznici(this.state.pageNo + 1)} >
            Sledeca
          </Button>
        </ButtonGroup>

        <Table bordered striped style={{ marginTop: 5 }}>
          <thead className="thead-dark">
            <tr>
              <th>Ime</th>
              <th>Prezime</th>
              <th>Godina Rodjenja</th>
              <th>Mesto</th>
              <th>Naziv auto skole</th>
              <th>Polozio</th>
              <th colSpan={2}></th>
            </tr>
          </thead>
          <tbody>
            {this.state.polaznici.map((polaznik) => {
              if (!polaznik.polozio) {
                this.state.ispisiPolozio = 'NE';
              }
              else{
                this.state.ispisiPolozio = 'DA';
              }
              return (
                <tr key={polaznik.id}>
                  <td>{polaznik.ime}</td>
                  <td>{polaznik.prezime}</td>
                  <td>{polaznik.godinaRodjenja}</td>
                  <td>{polaznik.mesto}</td>
                  <td>{polaznik.autoSkolaNaziv}</td>
                  <td>{this.state.ispisiPolozio}</td>
                  <td><Button
                    onClick={() => this.odslusaoTeoriju(polaznik.id)}
                    hidden={polaznik.odslusaoTeoriju}
                    variant="info"
                  >
                    Odslusao Teoriju
                  </Button>
                    <Button
                      onClick={() => this.odradioVoznju(polaznik.id)}
                      hidden={!polaznik.odslusaoTeoriju || polaznik.odradioVoznju}
                      variant="info"
                    >
                      Odradio voznju
                    </Button>
                    {console.log(polaznik.id)}
                    {this.state.prijavljeni.map((prijavljen) => {
                      
                    console.log(prijavljen.id)
                    })}
                    <Button
                      onClick={() => this.goToPrijava(polaznik.autoSkolaId,polaznik.id)}
                      hidden={!polaznik.odradioVoznju || polaznik.polozio }
                      variant="info"
                    >
                      
                      Prijava za polaganje
                    </Button>
                    <Button

                      onClick={() => this.polozio(polaznik.id)}
                      hidden={!polaznik.odradioVoznju}
                      disabled={polaznik.polozio}
                      variant="info"
                    >
                      Polozio
                    </Button></td>
                  {window.localStorage['role'] == "ROLE_ADMIN" ? [
                    <td>

                      <Button style={{ margin: 3, width: 150 }} variant="danger" onClick={() => this.delete(polaznik.id)}>Obrisi polaznika</Button>
                    </td>
                  ] : null}
                </tr>
              );
            })}

          </tbody>
        </Table>
      </div>
    );
  }
}

export default withNavigation(Glavna);
