package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.PolaganjeDto;
import test.model.Polaganje;

@Component
public class PolaganjeToPolaganjeDto implements Converter<Polaganje,PolaganjeDto>{
	
	@Override
	public PolaganjeDto convert(Polaganje polaganje) {
		PolaganjeDto dto = new PolaganjeDto();
		dto.setId(polaganje.getId());
		dto.setBrMesta(polaganje.getBrMesta());
		dto.setDatum(polaganje.getDatum().toString());
		dto.setAutoSkolaId(polaganje.getAutoSkola().getId());
		dto.setAutoSkolaNaziv(polaganje.getAutoSkola().getNaziv());
		
		return dto;
	}
	
	public List<PolaganjeDto> convert(List<Polaganje> polaganje){
		List<PolaganjeDto> retVal = new ArrayList<>();
		
		for(Polaganje p : polaganje) {
			PolaganjeDto dto = convert(p);
			retVal.add(dto);
		}
		
		return retVal;
	}

}
