package test.support;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.PolaznikDto;
import test.model.Polaznik;
import test.service.AutoSkolaService;
import test.service.PolaznikService;

@Component
public class PolaznikDtoToPolaznik implements Converter<PolaznikDto,Polaznik>{
	
	@Autowired
    private PolaznikService polaznikService;

    @Autowired
    private AutoSkolaService autoSkolaService;

    @Override
    public Polaznik convert(PolaznikDto dto) {
        Polaznik polaznik;

        if(dto.getId() == null){
            polaznik = new Polaznik();
        }else{
            Optional<Polaznik> optional = polaznikService.findOneById(dto.getId());
            polaznik=optional.get();
        }

        if(polaznik != null){
        	polaznik.setIme(dto.getIme());
        	polaznik.setPrezime(dto.getPrezime());
        	polaznik.setGodinaRodjenja(dto.getGodinaRodjenja());
        	polaznik.setMesto(dto.getMesto());
        	polaznik.setAutoSkola(autoSkolaService.findOneById(dto.getAutoSkolaId()));
        	polaznik.setOdslusaoTeoriju(dto.isOdslusaoTeoriju());
        	polaznik.setOdradioVoznju(dto.isOdradioVoznju());
        	polaznik.setPolozio(dto.isPolozio());
        }
        return polaznik;
    }

    public List<Polaznik> convert(List<PolaznikDto> polazniciDto){
		List<Polaznik> retVal = new ArrayList<>();
		
		for(PolaznikDto p : polazniciDto) {
			Polaznik polaznik = convert(p);
			retVal.add(polaznik);
		}
		
		return retVal;
	}

   

}
