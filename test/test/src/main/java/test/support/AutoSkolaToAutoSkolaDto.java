package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.AutoSkolaDto;
import test.model.AutoSkola;

@Component
public class AutoSkolaToAutoSkolaDto implements Converter<AutoSkola, AutoSkolaDto>{
	
		
		@Override
		public AutoSkolaDto convert(AutoSkola autoSkola) {
			AutoSkolaDto dto = new AutoSkolaDto();
			dto.setId(autoSkola.getId());
			dto.setNaziv(autoSkola.getNaziv());
			dto.setGodinaOsnivanja(autoSkola.getGodinaOsnivanja());
			dto.setBrVozila(autoSkola.getBrVozila());
			
			return dto;
		}
		
		public List<AutoSkolaDto> convert(List<AutoSkola> autoSkola){
			List<AutoSkolaDto> retVal = new ArrayList<>();
			
			for(AutoSkola a : autoSkola) {
				AutoSkolaDto dto = convert(a);
				retVal.add(dto);
			}
			
			return retVal;
		}

	}
