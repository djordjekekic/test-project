package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.PolaznikDto;
import test.model.Polaznik;

@Component
public class PolaznikToPolaznikDto implements Converter <Polaznik,PolaznikDto>{
	
	@Override
	public PolaznikDto convert(Polaznik polaznik) {
		PolaznikDto dto = new PolaznikDto();
		dto.setId(polaznik.getId());
		dto.setIme(polaznik.getIme());
		dto.setPrezime(polaznik.getPrezime());
		dto.setGodinaRodjenja(polaznik.getGodinaRodjenja());
		dto.setMesto(polaznik.getMesto());
		dto.setAutoSkolaId(polaznik.getAutoSkola().getId());
		dto.setAutoSkolaNaziv(polaznik.getAutoSkola().getNaziv());
		dto.setOdslusaoTeoriju(polaznik.isOdslusaoTeoriju());
		dto.setOdradioVoznju(polaznik.isOdradioVoznju());
		dto.setPolozio(polaznik.isPolozio());
		
		
		return dto;
	}
	
	public List<PolaznikDto> convert(List<Polaznik> polaznik){
		List<PolaznikDto> retVal = new ArrayList<>();
		
		for(Polaznik p : polaznik) {
			PolaznikDto dto = convert(p);
			retVal.add(dto);
		}
		
		return retVal;
	}

}
