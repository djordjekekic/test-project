package test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AutoSkola {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String naziv;
	
	@Column
	private int godinaOsnivanja;
	
	@Column
	private int brVozila;

	public AutoSkola(Long id, String naziv, int godinaOsnivanja, int brVozila) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godinaOsnivanja = godinaOsnivanja;
		this.brVozila = brVozila;
	}

	public AutoSkola() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getGodinaOsnivanja() {
		return godinaOsnivanja;
	}

	public void setGodinaOsnivanja(int godinaOsnivanja) {
		this.godinaOsnivanja = godinaOsnivanja;
	}

	public int getBrVozila() {
		return brVozila;
	}

	public void setBrVozila(int brVozila) {
		this.brVozila = brVozila;
	}
	
	
	

}
