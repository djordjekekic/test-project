package test.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Polaganje {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private int brMesta;
	@Column(nullable = false)
	private LocalDate datum;
	
	@ManyToOne
	@PrimaryKeyJoinColumn
	private AutoSkola autoSkola;

	public Polaganje(Long id, int brMesta, LocalDate datum, AutoSkola autoSkola) {
		super();
		this.id = id;
		this.brMesta = brMesta;
		this.datum = datum;
		this.autoSkola = autoSkola;
	}

	public Polaganje() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrMesta() {
		return brMesta;
	}

	public void setBrMesta(int brMesta) {
		this.brMesta = brMesta;
	}

	public LocalDate getDatum() {
		return datum;
	}

	public void setDatum(LocalDate datum) {
		this.datum = datum;
	}

	public AutoSkola getAutoSkola() {
		return autoSkola;
	}

	public void setAutoSkola(AutoSkola autoSkola) {
		this.autoSkola = autoSkola;
	}

	@Override
	public String toString() {
		return "Polaganje [id=" + id + ", brMesta=" + brMesta + ", datum=" + datum + ", autoSkola=" + autoSkola + "]";
	}
	
	

}
