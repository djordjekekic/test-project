package test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * this is my first project
 * it's test project for everything that i'll try to make
 * theme of this project is to make evidention of students in different driving license schools
 * with this project i will try to make more branches and different commits, 
 * so i can learn more about using git
 * also i will use this project to test every new idea, and different variants of coding*/
@SpringBootApplication
public class TestApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		 SpringApplication.run(TestApplication.class, args);
	}

}
