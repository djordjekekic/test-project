package test.service;

import java.util.List;
import java.util.Optional;

import test.model.Polaganje;

public interface PolaganjeService {
	
	List<Polaganje> findByAutoSkolaId(Long id);
	Optional<Polaganje> findOneById(Long id);
	Polaganje update(Polaganje polaganje);

}
