package test.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import test.model.Polaznik;
import test.repository.PolaznikRepository;
import test.service.PolaznikService;

@Service
public class JpaPolaznikService implements PolaznikService{

	@Autowired
	PolaznikRepository polaznikRepository;
	
	@Override
	public Optional<Polaznik> findOneById(Long id) {
		return polaznikRepository.findById(id);
	}

	@Override
	public Page<Polaznik> findAllPageable(Integer pageNo) {
		return polaznikRepository.findAll(PageRequest.of(pageNo, 3));
	}

	@Override
	public Polaznik save(Polaznik polaznik) {
		return polaznikRepository.save(polaznik);
	}

	@Override
	public Polaznik update(Polaznik polaznik) {
		return polaznikRepository.save(polaznik);
	}

	@Override
    public Polaznik delete(Long id) {
		Optional<Polaznik> optional = findOneById(id);
		Polaznik polaznik = optional.get();
        if(polaznik != null){
            polaznikRepository.delete(polaznik);
            return polaznik;
        }	
        return null;
    }

	@Override
	public Page<Polaznik> filter(Long autoSkolaId, String imePolaznika, Integer pageNo) {
			if(imePolaznika != null) {
				imePolaznika = "%"+imePolaznika+"%";
			}
			return polaznikRepository.filter(autoSkolaId, imePolaznika, PageRequest.of(pageNo, 3));
		}

}
