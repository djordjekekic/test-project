package test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.model.AutoSkola;
import test.repository.AutoSkolaRepository;
import test.service.AutoSkolaService;

@Service
public class JpaAutoSkolaService implements AutoSkolaService{
	
	@Autowired
	AutoSkolaRepository autoSkolaRepository;

	@Override
	public List<AutoSkola> findAll() {
		return autoSkolaRepository.findAll();
	}

	@Override
	public AutoSkola findOneById(Long id) {
		return autoSkolaRepository.findOneById(id);
	}

}
