package test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.model.Polaganje;
import test.repository.PolaganjeRepository;
import test.service.PolaganjeService;


@Service
public class JpaPolaganjeService implements PolaganjeService{
	
	@Autowired
	PolaganjeRepository polaganjeRepository;

	@Override
	public List<Polaganje> findByAutoSkolaId(Long id) {
		return polaganjeRepository.findByAutoSkolaId(id);
	}

	@Override
	public Optional<Polaganje> findOneById(Long id) {
		return polaganjeRepository.findById(id);
	}

	@Override
	public Polaganje update(Polaganje polaganje) {
		return polaganjeRepository.save(polaganje);
	}

}
