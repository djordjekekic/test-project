package test.service;

import java.util.List;

import test.model.AutoSkola;

public interface AutoSkolaService {
	
	List<AutoSkola> findAll();
	AutoSkola findOneById(Long id);

}
