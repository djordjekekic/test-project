package test.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import test.model.Polaznik;

public interface PolaznikService {
	
	Optional<Polaznik> findOneById(Long id);
	Page<Polaznik> findAllPageable(Integer pageNo);
	Polaznik save(Polaznik polaznik);
	Polaznik update(Polaznik polaznik);
	Polaznik delete(Long id);
	Page<Polaznik> filter(Long autoSkolaId,String imePolaznika, Integer pageNo);

}
