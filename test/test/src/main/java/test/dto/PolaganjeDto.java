package test.dto;

import javax.validation.constraints.Min;

public class PolaganjeDto {
	
	private Long id;
	
	@Min(value = 0)
	private int brMesta;
	private String datum;
	private Long autoSkolaId;
	private String autoSkolaNaziv;
	
	public PolaganjeDto() {
		super();
	}
	
	public PolaganjeDto(Long id, int brMesta, String datum, Long autoSkolaId, String autoSkolaNaziv) {
		super();
		this.id = id;
		this.brMesta = brMesta;
		this.datum = datum;
		this.autoSkolaId = autoSkolaId;
		this.autoSkolaNaziv = autoSkolaNaziv;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public Long getAutoSkolaId() {
		return autoSkolaId;
	}
	public void setAutoSkolaId(Long autoSkolaId) {
		this.autoSkolaId = autoSkolaId;
	}
	public String getAutoSkolaNaziv() {
		return autoSkolaNaziv;
	}
	public void setAutoSkolaNaziv(String autoSkolaNaziv) {
		this.autoSkolaNaziv = autoSkolaNaziv;
	}

	public int getBrMesta() {
		return brMesta;
	}

	public void setBrMesta(int brMesta) {
		this.brMesta = brMesta;
	}
	
	

}
