package test.dto;

import org.hibernate.validator.constraints.Length;

public class PolaznikDto {
	
	private Long id;
	
	@Length(max = 30)
	private String ime;
	private String prezime;
	private int godinaRodjenja;
	private String mesto;
	private Long autoSkolaId;
	private String autoSkolaNaziv;
	private boolean odslusaoTeoriju;
	private boolean odradioVoznju;
	private boolean polozio;
	//private String polozioIspis;
	
	public PolaznikDto(Long id, String ime, String prezime, int godinaRodjenja, String mesto, Long autoSkolaId,
			String autoSkolaNaziv, boolean odslusaoTeoriju, boolean odradioVoznju, boolean polozio, String polozioIspis) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.godinaRodjenja = godinaRodjenja;
		this.mesto = mesto;
		this.autoSkolaId = autoSkolaId;
		this.autoSkolaNaziv = autoSkolaNaziv;
		this.odslusaoTeoriju = odslusaoTeoriju;
		this.odradioVoznju = odradioVoznju;
		this.polozio = polozio;
		//this.polozioIspis = polozioIspis;	
		}

	public PolaznikDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public int getGodinaRodjenja() {
		return godinaRodjenja;
	}

	public void setGodinaRodjenja(int godinaRodjenja) {
		this.godinaRodjenja = godinaRodjenja;
	}

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public Long getAutoSkolaId() {
		return autoSkolaId;
	}

	public void setAutoSkolaId(Long autoSkolaId) {
		this.autoSkolaId = autoSkolaId;
	}

	public String getAutoSkolaNaziv() {
		return autoSkolaNaziv;
	}

	public void setAutoSkolaNaziv(String autoSkolaNaziv) {
		this.autoSkolaNaziv = autoSkolaNaziv;
	}

	public boolean isOdslusaoTeoriju() {
		return odslusaoTeoriju;
	}

	public void setOdslusaoTeoriju(boolean odslusaoTeoriju) {
		this.odslusaoTeoriju = odslusaoTeoriju;
	}

	public boolean isOdradioVoznju() {
		return odradioVoznju;
	}

	public void setOdradioVoznju(boolean odradioVoznju) {
		this.odradioVoznju = odradioVoznju;
	}

	public boolean isPolozio() {
		return polozio;
	}

	public void setPolozio(boolean polozio) {
		this.polozio = polozio;
	}

	/*public String getPolozioIspis() {
		polozioIspis="NE";
		if(polozio==true) {
			polozioIspis="DA";
		}
		return polozioIspis;
	}*/

	/*public void setPolozioIspis(boolean polozio) {
		String polozioIspis="NE";
		if(polozio==true) {
			polozioIspis="DA";
		}
		this.polozioIspis = polozioIspis;
	}*/

}
