package test.dto;

import org.hibernate.validator.constraints.Length;

public class AutoSkolaDto {
	
	private Long id;
	
	@Length(max = 50)
	private String naziv;
	private int godinaOsnivanja;
	private int brVozila;
	
	public AutoSkolaDto() {
		super();
	}
	public AutoSkolaDto(Long id, String naziv, int godinaOsnivanja, int brVozila) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godinaOsnivanja = godinaOsnivanja;
		this.brVozila = brVozila;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public int getGodinaOsnivanja() {
		return godinaOsnivanja;
	}
	public void setGodinaOsnivanja(int godinaOsnivanja) {
		this.godinaOsnivanja = godinaOsnivanja;
	}
	public int getBrVozila() {
		return brVozila;
	}
	public void setBrVozila(int brVozila) {
		this.brVozila = brVozila;
	}
	
	

}
