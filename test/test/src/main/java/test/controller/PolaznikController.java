package test.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.dto.PolaznikDto;
import test.model.Polaznik;
import test.service.PolaznikService;
import test.support.PolaznikDtoToPolaznik;
import test.support.PolaznikToPolaznikDto;

@RestController
@RequestMapping("api/polaznici")
public class PolaznikController {
	
	@Autowired
	PolaznikToPolaznikDto toPolaznikDto;
	
	@Autowired
	PolaznikDtoToPolaznik toPolaznik;;
	
	@Autowired
	PolaznikService polaznikService;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping()
	public ResponseEntity<List<PolaznikDto>> pageable(
			@RequestParam(value = "autoSkola", required = false)Long autoSkolaId,//value upisujemo u vsc kao params
			@RequestParam(value = "imePolaznika", required = false)String imePolaznika,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		Page<Polaznik> polaznici = null;
		
		if (autoSkolaId != null || imePolaznika != null) {
			polaznici = polaznikService.filter(autoSkolaId,imePolaznika,pageNo);
		} else {
			polaznici = polaznikService.findAllPageable(pageNo);
		}

		List<PolaznikDto> polazniciDto = toPolaznikDto.convert(polaznici.getContent());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(polaznici.getTotalPages()));

		return new ResponseEntity<>(polazniciDto, headers, HttpStatus.OK);
	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
//	@GetMapping()
//	public ResponseEntity<List<PolaznikDto>> pageable(@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
//
//		Page<Polaznik> polaznici = polaznikService.findAllPageable(pageNo);
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.add("Total-Pages", Integer.toString(polaznici.getTotalPages()));
//
//		return new ResponseEntity<>(toPolaznikDto.convert(polaznici.getContent()), headers, HttpStatus.OK);
//	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	ResponseEntity<PolaznikDto> getOne(@PathVariable Long id) {
		Optional<Polaznik> optional= polaznikService.findOneById(id);
		if (!optional.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toPolaznikDto.convert(optional.get()), HttpStatus.OK);
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<PolaznikDto> create(@Validated @RequestBody PolaznikDto polaznikDto) {

		Polaznik polaznik = toPolaznik.convert(polaznikDto);
		polaznik.setOdradioVoznju(false);
		polaznik.setOdslusaoTeoriju(false);
		polaznik.setPolozio(false);
		polaznikService.save(polaznik);

		return new ResponseEntity<>(toPolaznikDto.convert(polaznik), HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	public ResponseEntity<PolaznikDto> update(@PathVariable Long id, @Validated @RequestBody PolaznikDto polaznikDto ) {

		if (!id.equals(polaznikDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Polaznik sacuvanPolaznik = polaznikService.update(toPolaznik.convert(polaznikDto));

		return new ResponseEntity<>(toPolaznikDto.convert(sacuvanPolaznik), HttpStatus.OK);
	}
	
	@PutMapping(value = "teorija/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	public ResponseEntity<PolaznikDto> teorija(@PathVariable Long id) {
		
		Optional<Polaznik> optional = polaznikService.findOneById(id);
		Polaznik polaznik = optional.get();
		polaznik.setOdslusaoTeoriju(true);
		
		polaznikService.update(polaznik);

		return new ResponseEntity<>(toPolaznikDto.convert(polaznik), HttpStatus.OK);
	}
	
	@PutMapping(value = "voznja/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	public ResponseEntity<PolaznikDto> voznja(@PathVariable Long id) {
		
		Optional<Polaznik> optional = polaznikService.findOneById(id);
		Polaznik polaznik = optional.get();
		polaznik.setOdradioVoznju(true);
		
		polaznikService.update(polaznik);

		return new ResponseEntity<>(toPolaznikDto.convert(polaznik), HttpStatus.OK);
	}
	
	@PutMapping(value = "polozio/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	public ResponseEntity<PolaznikDto> polozio(@PathVariable Long id) {
		
		Optional<Polaznik> optional = polaznikService.findOneById(id);
		Polaznik polaznik = optional.get();
		polaznik.setPolozio(true);
		
		polaznikService.update(polaznik);

		return new ResponseEntity<>(toPolaznikDto.convert(polaznik), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<PolaznikDto> delete(@PathVariable Long id) {
		Optional<Polaznik> optional = polaznikService.findOneById(id);
		if (optional.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		PolaznikDto zaIspis = toPolaznikDto.convert(optional.get());
		polaznikService.delete(id);
		return new ResponseEntity<>(zaIspis, HttpStatus.OK);
	}

}
