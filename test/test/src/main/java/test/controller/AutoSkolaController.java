package test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.dto.AutoSkolaDto;
import test.dto.PolaganjeDto;
import test.model.AutoSkola;
import test.model.Polaganje;
import test.service.AutoSkolaService;
import test.service.PolaganjeService;
import test.support.AutoSkolaToAutoSkolaDto;
import test.support.PolaganjeToPolaganjeDto;

@RestController
@RequestMapping("api/auto-skola")
public class AutoSkolaController {
	
	@Autowired
	private AutoSkolaService autoSkolaService;
	
	@Autowired
	private AutoSkolaToAutoSkolaDto toAutoSKolaDto;
	
	@Autowired
	private PolaganjeToPolaganjeDto toPolaganjeDto;
	
	@Autowired
	private PolaganjeService polaganjeService;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping()
	public ResponseEntity<List<AutoSkolaDto>> getAll ()
	{
		
		List<AutoSkola> autoSkole = autoSkolaService.findAll();
		List<AutoSkolaDto> autoSkoleDto = new ArrayList<>();
		for(AutoSkola autoSkola : autoSkole) {
			AutoSkolaDto autoSkolaDto = toAutoSKolaDto.convert(autoSkola);
			autoSkoleDto.add(autoSkolaDto);
		}
		
		return new ResponseEntity<>(autoSkoleDto,HttpStatus.OK);
	}
	
//	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
//	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
//	ResponseEntity<AutoSkolaDto> getOne(@PathVariable Long id) {
//		AutoSkola as= autoSkolaService.findOneById(id);
//		
//		return new ResponseEntity<>(toAutoSKolaDto.convert(as), HttpStatus.OK);
//	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@RequestMapping(value = "/{id}/polaganja", method = RequestMethod.GET)
	ResponseEntity<List<PolaganjeDto>> getByAutoSkola(@PathVariable Long id,
													@RequestParam(value = "autoSkola", required = false)Long autoSkolaId) {
		autoSkolaId=id;
		List<Polaganje> lista = polaganjeService.findByAutoSkolaId(autoSkolaId);
		List<PolaganjeDto> listaDto = new ArrayList<>();
		for(int i = 0; i<lista.size();i++) {
			PolaganjeDto dto = toPolaganjeDto.convert(lista.get(i));
			listaDto.add(dto);
		}
		return new ResponseEntity<>(listaDto,HttpStatus.OK);
	}

}
