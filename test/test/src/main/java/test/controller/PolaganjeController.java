package test.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.dto.PolaganjeDto;
import test.dto.PolaznikDto;
import test.model.Polaganje;
import test.model.Polaznik;
import test.service.PolaganjeService;
import test.service.PolaznikService;
import test.support.PolaganjeToPolaganjeDto;
import test.support.PolaznikToPolaznikDto;

@RestController
@RequestMapping("api/polaganje")
public class PolaganjeController {
	
	@Autowired
	private PolaganjeService polaganjeService;
	
	@Autowired
	private PolaganjeToPolaganjeDto toPolaganjeDto;
	
	@Autowired
	private PolaznikToPolaznikDto toPolaznikDto;
	
	@Autowired
	private PolaznikService polaznikService;
	
	private List<Polaznik> polaznici = new ArrayList <> (); 
	
	@PutMapping(value = "prijava/{id}/{idPolaznik}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	public ResponseEntity<PolaganjeDto> prijava(@PathVariable Long id, @PathVariable Long idPolaznik) {
		
		Optional<Polaganje> optional = polaganjeService.findOneById(id);
		Polaganje polaganje=optional.get();
		polaganje.setBrMesta(polaganje.getBrMesta()-1);
		
		Optional<Polaznik> polaznikOptional = polaznikService.findOneById(idPolaznik);
		Polaznik polaznik = polaznikOptional.get();
		
		for(int i = 0; i<polaznici.size();i++) {
			if(polaznici.get(i).getId()==polaznik.getId())
		return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);	
		}
		
		polaznici.add(polaznik);
		polaganjeService.update(polaganje);

		return new ResponseEntity<>(toPolaganjeDto.convert(polaganje), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping(value = "/prijavljeni")
	public ResponseEntity<List<PolaznikDto>> getAll ()
	{
		
		List<PolaznikDto> polazniciDto = new ArrayList<>();
		for(Polaznik polaznik : polaznici) {
			PolaznikDto polaznikDto = toPolaznikDto.convert(polaznik);
			polazniciDto.add(polaznikDto);
		}
		
		return new ResponseEntity<>(polazniciDto,HttpStatus.OK);
	}

}
