package test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import test.model.Polaganje;

@Repository
public interface PolaganjeRepository extends JpaRepository<Polaganje,Long>{
	
	@Query("SELECT p FROM Polaganje p WHERE auto_skola_id = :autoSkolaId")
	List<Polaganje> findByAutoSkolaId(@Param("autoSkolaId") Long autoSkolaId);

}
