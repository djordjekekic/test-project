package test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import test.model.Polaznik;

@Repository
public interface PolaznikRepository extends JpaRepository<Polaznik,Long>{

	@Query("SELECT p FROM Polaznik p WHERE (:autoSkolaId = NULL OR auto_skola_id = :autoSkolaId) AND (:imePolaznika = NULL OR ime LIKE :imePolaznika)")
	Page<Polaznik> filter(@Param("autoSkolaId") Long autoSkolaId,@Param("imePolaznika") String imePolaznika, Pageable pageable);

}
