INSERT INTO `user` (id, username, password, role)
              VALUES (1,'miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO `user` (id, username, password, role)
              VALUES (2,'tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');
INSERT INTO `user` (id, username, password, role)
              VALUES (3,'petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
             
INSERT INTO auto_skola (id,br_vozila,godina_osnivanja,naziv) VALUES (1,5,2000,'EURO');
INSERT INTO auto_skola (id,br_vozila,godina_osnivanja,naziv) VALUES (2,3,2010,'VELEMIR');
INSERT INTO auto_skola (id,br_vozila,godina_osnivanja,naziv) VALUES (3,10,2005,'ALUNO');

INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (1,10,'2022-08-10',1);
INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (2,7,'2022-08-05',2);
INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (3,5,'2022-08-15',3);
INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (4,4,'2022-08-09',1);
INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (5,6,'2022-08-30',2);
INSERT INTO polaganje (id,br_mesta,datum,auto_skola_id) VALUES (6,8,'2022-08-22',3);

INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(1,'Djordje','Kekic',1990,'Bac',1,true,true,true);
INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(2,'Branko','Brankovic',1993,'Novi Sad',2,true,true,false);
INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(3,'Marko','Markovic',1991,'Novi Sad',3,true,false,false);
INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(4,'Nikola','Nikolic',1990,'Bac',1,true,true,false);
INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(5,'Dragan','Draganic',2000,'Bac',2,true,true,true);
INSERT INTO polaznik (id,ime,prezime,godina_rodjenja,mesto,auto_skola_id,odslusao_teoriju,odradio_voznju,polozio) 
VALUES(6,'Sima','Simic',1998,'Novi Sad',3,false,false,false);